/*
 * Heavily leveraged this package by Anthony Elder:
 * https://github.com/HarringayMakerSpace/awsiot/blob/master/Esp8266AWSIoTExample/Esp8266AWSIoTExample.ino
 */
#include <LiquidCrystal.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <AceButton.h>
#include "certs.h"

using namespace ace_button;
#define BUTTON_PIN D4

#define LINE_LENGTH 16

LiquidCrystal lcd(D1, D2, D3, D5, D6, D7);

AceButton wifiResetButton(BUTTON_PIN);
bool withResetButton = true; // set to false if you don't want a reset button

WiFiManager wifiManager;
const char* accessPointName = "WaldoWifi";
int wifiConnected = 0;

// Add your API endpoint here. You can get this from the output of the
// CloudFormation stack, from the output called "iotEndpoint".
const char* iotEndpoint = "YOUR_ENDPOINT_HERE";

BearSSL::X509List client_crt(certificatePemCrt);
BearSSL::PrivateKey client_key(privatePemKey);
BearSSL::X509List rootCert(caPemCrt);

// can't use wifi-manager here, since it doesn't play well with the certs
WiFiClientSecure wiFiClient;
void msgReceived(char* topic, byte* payload, unsigned int len);
PubSubClient pubSubClient(iotEndpoint, 8883, msgReceived, wiFiClient);

unsigned long lastPublish;
int msgCount;
int refreshAttempts = 0;

void(* resetDevice) (void) = 0;

int setConnected() {
  Serial.println("Wifi configured, resetting.");
  displayText("WiFi configured, resetting", 1);
  resetDevice();
}

int connectToWifi() {
  Serial.println("Attempting to connect to wifi");
  displayText("Connecting to Wifi...", 1);
//  wifiManager.resetSettings(); // clears wifi settings for testing
  wifiManager.setConfigPortalBlocking(false);
  wifiManager.setSaveConfigCallback(setConnected);
  if (wifiManager.autoConnect(accessPointName)) {
    Serial.println("Connected to wifi");
    displayText("Connected");
    return 1;
  } else {
    Serial.println("Wifi connection failed, starting access point.");
    displayText("Connect to \"" + String(accessPointName) + "\"");
    return 0;
  }
}

int nearestSpaceToCenter(String message) {
  const int len = message.length();
  if (len <= 4) {
    return -1;
  }
  const int center = ceil(len / 2) - 1;
  for (int delta = 0; delta <= center; delta++) {
    if (message[center - delta] == ' ') {
      return center - delta;
    }
    if (message[center + delta] == ' ') {
      return center + delta;
    }
  }
  return len > LINE_LENGTH ? LINE_LENGTH - 1 : -1;
}

/**
 * Finds the index to start printing a string
 * such that it's centered in the display.
 */
int getStartIndex(String message) {
  return max(0, int(floor((LINE_LENGTH - message.length()) / 2)));
}

void displayText(String message) {
  displayText(message, 0);
}

void displayText(String message, int blinkCursor) {
  lcd.clear();
  message.trim();
  
  const int manualSplitNdx = message.indexOf('|');
  const int splitNdx = manualSplitNdx > 0 ? manualSplitNdx : nearestSpaceToCenter(message);
  String line1 = message;
  String line2 = "";
  if (splitNdx > 0) {
    line1 = message.substring(0, splitNdx);
    line2 = message.substring(splitNdx + 1);
  }
  line1.trim();
  line2.trim();
  
  Serial.print("Displaying message: ");
  Serial.print(line1);
  Serial.print("|");
  Serial.println(line2);
  
  lcd.setCursor(getStartIndex(line1), 0);
  lcd.print(line1);
  lcd.setCursor(getStartIndex(line2), 1);
  lcd.print(line2);
  if (blinkCursor) {
    lcd.blink();
  } else {
    lcd.noBlink();
  }
}

void msgReceived(char* topic, byte* payload, unsigned int length) {
  if (String(topic).indexOf("rejected") >= 0) {
    Serial.println("Something went wrong with the Shadow update!");
  }
  char json[length + 1];
  Serial.print("Message received on "); Serial.print(topic); Serial.print(": ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    json[i] = payload[i];
  }
  json[length] = '\0';
  Serial.println();
  Serial.print("JSON: ");
  Serial.println(json);
  StaticJsonDocument<200> jsonData;
  DeserializationError error = deserializeJson(jsonData, json);
  if (error) {
    Serial.println("JSON parse error");
    return;
  }
  const char* location = jsonData["state"]["desired"]["location"];
  Serial.print("Location: ");
  Serial.println(location);
  displayText(location);
}

void pubSubCheckConnect() {
  if (!pubSubClient.connected()) {
    displayText("Connecting to AWS IoT", 1);
    Serial.print("PubSubClient connecting to AWS: ");
    Serial.println(iotEndpoint);
    while (!pubSubClient.connected()) {
      pubSubClient.connect("WaldoDevice");
    }
    Serial.println("Connected, subscribing to topics.");
    pubSubClient.subscribe("$aws/things/WaldoDevice/shadow/update/accepted");
    pubSubClient.subscribe("$aws/things/WaldoDevice/shadow/get/accepted");
    displayText("Connected");
    triggerRefresh();
  }
  pubSubClient.loop();
}

// TODO there's gotta be a way to speed this up
void setCurrentTime() {
  displayText("Getting current time", 1);
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");

  Serial.println("Waiting for NTP time sync");
  time_t now = time(nullptr);
  while (now < 8 * 3600 * 2) {
    delay(500);
    now = time(nullptr);
  }
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print("Current time: ");
  Serial.println(asctime(&timeinfo));
}

void triggerRefresh() {
  Serial.println("Triggering refresh");
  pubSubClient.publish("$aws/things/WaldoDevice/shadow/get", "");
}

void handleResetButton(AceButton* button, uint8_t eventType, uint8_t buttonState) {
  if (eventType == AceButton::kEventPressed) {
    Serial.println("Restting WiFi manager saved settings and the device");
    displayText("Performing full reset", 1);
    wifiManager.resetSettings();
    resetDevice();
  }
}

void setup() {
  Serial.begin(9600);
  lcd.begin(16,2);

  if (withResetButton) {
    pinMode(BUTTON_PIN, INPUT_PULLUP);
    wifiResetButton.setEventHandler(handleResetButton);
  }

  wifiConnected = connectToWifi();

  if (wifiConnected) {
    String ssid = wifiManager.getWiFiSSID();
    String password = wifiManager.getWiFiPass();
  
    Serial.print("Connecting to network: ");
    Serial.print(ssid);
    displayText("Securing connection", 1);
    WiFi.begin(ssid, password);
    WiFi.waitForConnectResult();
    Serial.println("WiFi connected");
  
    // set current time, otherwise certificates are flagged as expired
    setCurrentTime();

    // not supported by WiFi manager, unfortunately
    wiFiClient.setClientRSACert(&client_crt, &client_key);
    wiFiClient.setTrustAnchors(&rootCert);

    // increase buffer size for MQTT PubSub client to deal with
    // AWS IoT's large messages to the "/shadow/get/approved" topic
    pubSubClient.setBufferSize(512); // TODO 512 is probably overkill
  }
}

void loop() {
  wifiManager.process();
  if (withResetButton) {
    wifiResetButton.check();
  }
  if (wifiConnected) {
    pubSubCheckConnect();
  }
}
